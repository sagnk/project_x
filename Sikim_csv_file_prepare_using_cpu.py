import pandas as pd
import numpy as np
import itertools
from datetime import datetime
from dateutil import parser
df_punjab_17_conf_h_n_winter = pd.read_csv('~/scratch/python/viirs_fire-master/data/processed/csv/punjab_17_conf_n_h_winter.csv').drop('Unnamed: 0',axis=1)
print(df_punjab_17_conf_h_n_winter.shape)
df_punjab_17_conf_h_n_winter['acq_date_epoch'] = df_punjab_17_conf_h_n_winter.acq_date.apply(lambda x:parser.parse(x).timestamp())
df_trunc = df_punjab_17_conf_h_n_winter[:]
df_trunc['id'] = df_trunc.index
combs = (itertools.combinations(df_trunc.id,2))
#com_length = len(combs)
print(df_trunc.head())
##list_id_coords = []
##list_fid_coords = []
##list_id_acq_date = []
##list_fid_acq_date = []
punjab_lat_values = df_trunc.latitude.values
punjab_lon_values = df_trunc.longitude.values
punjab_aqndatepoc_values = df_trunc.acq_date_epoch.values
file1 = open('sikim_multi/stage2cudacf.txt','w')
file = open('sikim_multi/text_punjab_comb_dataframe.txt','w')
l=0
file.write('Index')
file.write(',')
file.write('id_lat')
file.write(',')
file.write('id_lon')
file.write(',')
file.write('fid_lat')
file.write(',')
file.write('fid_lon')
file.write(',')
file.write('id_acq_date')
file.write(',')
file.write('fid_acq_date')
file.write('\n')
for lat_long_pair in combs:
##    temp_id_lat_long = np.float(df_trunc[df_trunc.id==lat_long_pair[0]].latitude.values[0]),np.float(df_trunc[df_trunc.id==lat_long_pair[0]].longitude.values[0])
##    temp_fid_lat_long = np.float(df_trunc[df_trunc.id==lat_long_pair[1]].latitude.values[0]),np.float(df_trunc[df_trunc.id==lat_long_pair[1]].longitude.values[0])
##    temp_id_lat_long = np.float32(punjab_lat_values[lat_long_pair[0]]),np.float32(punjab_lon_values[lat_long_pair[0]])
##    temp_fid_lat_long = np.float32(punjab_lat_values[lat_long_pair[1]]),np.float32(punjab_lon_values[lat_long_pair[1]])

    file.write(str(l))
    file.write(',')
    file.write(str(np.float32(punjab_lat_values[lat_long_pair[0]])))
    file.write(',')
    file.write(str(np.float32(punjab_lon_values[lat_long_pair[0]])))
    file.write(',')
    file.write(str(np.float32(punjab_lat_values[lat_long_pair[1]])))
    file.write(',')
    file.write(str(np.float32(punjab_lon_values[lat_long_pair[1]])))
    file.write(',')
    file.write(str(punjab_aqndatepoc_values[lat_long_pair[0]]))
    file.write(',')
    file.write(str(punjab_aqndatepoc_values[lat_long_pair[1]]))
    file.write('\n')
    if (l%1000000==0):
        file1.write(str(l))
        file1.write(',')
        file1.close()
        file1 = open('sikim_multi/stage2cudacf.txt','a')
        file.close()
        file = open('sikim_multi/text_punjab_comb_dataframe.txt','a')
##    list_id_coords.append(temp_id_lat_long)
##    list_fid_coords.append(temp_fid_lat_long)
##    list_id_acq_date.append(punjab_aqndatepoc_values[lat_long_pair[0]])
##    list_fid_acq_date.append(punjab_aqndatepoc_values[lat_long_pair[1]])
##    list_id_acq_date.append((df_trunc[df_trunc.id==lat_long_pair[0]].acq_date_epoch.values[0]))
##    list_fid_acq_date.append((df_trunc[df_trunc.id==lat_long_pair[1]].acq_date_epoch.values[0]))
    l=l+1
file.close()
file1.close()
print('Program Finished ....')
