import pandas as pd
from geopy import distance
import numpy as np
import itertools
from datetime import datetime
import xarray as xr
file = open('stage1.txt','w') 
df_punjab_17_conf_h_n_winter = pd.read_csv('~/scratch/python/viirs_fire-master/data/processed/csv/punjab_17_conf_n_h_winter.csv').drop('Unnamed: 0',axis=1)
df_punjab_17_conf_h_n_winter['id'] = df_punjab_17_conf_h_n_winter.index
#df_punjab_17_conf_h_n_winter = df_punjab_17_conf_h_n_winter[0:20]
xr_punjab_17_conf_h_n_winter = df_punjab_17_conf_h_n_winter.to_xarray()
ll = np.arange(len(df_punjab_17_conf_h_n_winter['id']))
combs = list(itertools.combinations(ll,2))
lenght_combinations = len(combs)
#print(lenght_combinations)
file.write('length of combination list:')
file.write(str(lenght_combinations))
file.close()
#df = pd.DataFrame(combs)
file = open('stage2.txt','w')
#df.to_csv('~/scratch/python/combination_list.csv')
#file.write('Combination list csv exported successfully')
list_id = []
list_fid = []
list_dist = []
file.write('\n Loop for distance computation starts:')
l=0
punjab_lat_values = xr_punjab_17_conf_h_n_winter.latitude.values
punjab_lon_values = xr_punjab_17_conf_h_n_winter.longitude.values
for lat_long_pair in combs:
    if (l%10000==0):
        file.write(str(l))
        file.write(',')
        file.close()
        file = open('stage2.txt','a')
    l=l+1
    temp_id_lat_long = float(punjab_lat_values[lat_long_pair[0]]),float(punjab_lon_values[lat_long_pair[0]])
    temp_fid_lat_long = float(punjab_lat_values[lat_long_pair[1]]),float(punjab_lon_values[lat_long_pair[1]])

    if (distance.geodesic(temp_id_lat_long,temp_fid_lat_long))<100:
        list_id.append(lat_long_pair[0])
        list_fid.append(lat_long_pair[1])
        temp_val = float(str(distance.great_circle(temp_id_lat_long,temp_fid_lat_long))[:-4])
        list_dist.append(round(temp_val,4))
file.write('\n Loop for distance computation finished')
dist_till_100km_dataset = xr.Dataset()
list_len = len(list_id)
file.write('\n List length for till 100km range:')
file.write(str(list_len))
file.close()
index_range = np.arange(list_len)
dist_till_100km_dataset['index'] = (('index'),index_range)
dist_till_100km_dataset['id'] = (('index'),list_id)
dist_till_100km_dataset['fid'] = (('index'),list_fid)
dist_till_100km_dataset['dist'] = (('index'),list_dist)
dist_till_100km_dataset.to_netcdf('~/scratch/python/dist_till_100km_dataset.nc')
file = open('stage3.txt','w')
file.write('Dataframe creation for distance computation Starts')
#df_punjab_17_conf_h_n_winter_dist_computed = pd.DataFrame(
#    {'id': list_id,
#     'fid': list_fid,
#     'dist': list_dist
#    })
#punjab_17_conf_h_n_winter_dist_computed = [list_id,list_fid,list_dist]
file.write('\n Dataframe creation for distance computation Finished')
file.write('\n File export for distance dataframe starts')
#df_punjab_17_conf_h_n_winter_dist_computed.to_csv('~/scratch/python/df_punjab_17_conf_h_n_winter_dist_computed.csv')
file.write('\n File export for distance dataframe Finished')
tm = pd.to_datetime(xr_punjab_17_conf_h_n_winter.acq_date.values)
time = [xr_punjab_17_conf_h_n_winter.index.values,tm]
time_diff_days = []
file.close()
file = open('stage4.txt','a')
file.write('\n The lenghth of list crated:')
file.write('\n Time difference loop starts:')
for i in range(0,list_len):
    file.write(str(i))
    file.write(',')
    if (l%10000==0):
        file.close()
        file = open('Stage4.txt','a')
    time_diff_days.append(abs((time[1][list_id[i]]-time[1][list_fid[i]])).days)
file.write('\n Time difference loop finishes ......')
file.close()
file = open('stage5.txt','a')
kset = xr.Dataset()
kset['index'] = (('index'),index_range)
kset['id'] = (('index'),list_id)
kset['fid'] = (('index'),list_fid)
kset['dist'] = (('index'),list_dist)
kset['time_diff_days'] = (('index'),time_diff_days)
kset.to_netcdf('~/scratch/python/punjab_17_conf_h_n_winter_dist_time_computed.nc')
file.write('\n File export of Dist time comp starts......')
#df_punjab_17_conf_h_n_winter_dist_computed.to_csv('~/scratch/python/df_punjab_17_conf_h_n_winter_dist_time_computed.csv')
file.write('\n program finished successfully ....')
file.close()
