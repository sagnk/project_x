import pandas as pd
from geopy import distance
import numpy as np
import itertools
from datetime import datetime
import xarray as xr
import time
import math
def dist(in_id,in_fid):
    temp = math.sqrt(((int(111*in_id[0])) - int(111*in_fid[0]))**2 + (((int(in_id[1]*96)) - int(in_fid[1]*96)))**2)
    return temp
def main_code():
    file = open('sikim_multi/stage1cf.txt','w') 
    df_punjab_17_conf_h_n_winter = pd.read_csv('~/scratch/python/viirs_fire-master/data/processed/csv/punjab_17_conf_n_h_winter.csv').drop('Unnamed: 0',axis=1)
    df_punjab_17_conf_h_n_winter['id'] = df_punjab_17_conf_h_n_winter.index
    #df_punjab_17_conf_h_n_winter = df_punjab_17_conf_h_n_winter[0:90]
    xr_punjab_17_conf_h_n_winter = df_punjab_17_conf_h_n_winter.to_xarray()
    ll = np.arange(len(df_punjab_17_conf_h_n_winter['id']))
    combs = list(itertools.combinations(ll,2))
    lenght_combinations = len(combs)
    #print(lenght_combinations)
    file.write('length of combination list:')
    file.write(str(lenght_combinations))
    file.close()
    #df = pd.DataFrame(combs)
    file = open('sikim_multi/stage2cf.txt','w')
    #df.to_csv('~/scratch/python/combination_list.csv')
    #file.write('Combination list csv exported successfully')
    list_id = []
    list_fid = []
    list_dist = []
    file.write('\n Loop for distance computation starts:')
    l=0
    punjab_lat_values = xr_punjab_17_conf_h_n_winter.latitude.values
    punjab_lon_values = xr_punjab_17_conf_h_n_winter.longitude.values
    for lat_long_pair in combs:
        if (l%100000==0):
            file.write(str(l))
            file.write(',')
            file.close()
            file = open('sikim_multi/stage2cf.txt','a')
        l=l+1
        temp_id_lat_long = float(punjab_lat_values[lat_long_pair[0]]),float(punjab_lon_values[lat_long_pair[0]])
        temp_fid_lat_long = float(punjab_lat_values[lat_long_pair[1]]),float(punjab_lon_values[lat_long_pair[1]])

        if ((dist(temp_id_lat_long,temp_fid_lat_long))<100):
            list_id.append(lat_long_pair[0])
            list_fid.append(lat_long_pair[1])
            temp_val = float(str(distance.great_circle(temp_id_lat_long,temp_fid_lat_long))[:-4])
            list_dist.append(round(temp_val,4))
            
##        if (distance.geodesic(temp_id_lat_long,temp_fid_lat_long))<100:
##            #print(distance.geodesic(temp_id_lat_long,temp_fid_lat_long))
##            list_id.append(lat_long_pair[0])
##            list_fid.append(lat_long_pair[1])
##            temp_val = float(str(distance.great_circle(temp_id_lat_long,temp_fid_lat_long))[:-4])
##            list_dist.append(round(temp_val,4))
    file.write('\n Loop for distance computation finished')
    file.close()
    file = open('sikim_multi/stage3cf.txt','w')
    file.write('Dataframe creation for distance computation Starts')
    df_punjab_17_conf_h_n_winter_dist_computed = pd.DataFrame(
        {'id': list_id,
         'fid': list_fid,
         'dist': list_dist
        })
    #punjab_17_conf_h_n_winter_dist_computed = [list_id,list_fid,list_dist]
    print(df_punjab_17_conf_h_n_winter_dist_computed.head())
    file.write('\n Dataframe creation for distance computation Finished')
    file.write('\n File export for distance dataframe starts')
    #df_punjab_17_conf_h_n_winter_dist_computed.to_csv('~/scratch/python/sikim_multi/df_cf_punjab_17_conf_h_n_winter_dist_computed.csv')
    file.write('\n File export for distance dataframe Finished')
    tm = pd.to_datetime(xr_punjab_17_conf_h_n_winter.acq_date.values)
    time = [xr_punjab_17_conf_h_n_winter.index.values,tm]
    time_diff_days = []
    file.close()
    file = open('sikim_multi/stage4cf.txt','w')
    file.write('\n Time difference loop starts:')
    list_len = len(list_id)
    for i in range(0,list_len):
        if (i%100000==0):
            file.write(str(i))
            file.write(',')
            file.close()
            file = open('sikim_multi/stage4cf.txt','a')
        time_diff_days.append(abs((time[1][list_id[i]]-time[1][list_fid[i]])).days)
    file.write('\n Time difference loop finishes ......')
    file.close()
    df_punjab_17_conf_h_n_winter_dist_computed['time_diff_days'] = time_diff_days
    print(df_punjab_17_conf_h_n_winter_dist_computed.head())
    file = open('sikim_multi/stage5cf.txt','w')
##    kset = xr.Dataset()
##    index_range = np.arange(list_len)
##    kset['index'] = (('index'),index_range)
##    kset['id'] = (('index'),list_id)
##    kset['fid'] = (('index'),list_fid)
##    kset['dist'] = (('index'),list_dist)
##    kset['time_diff_days'] = (('index'),time_diff_days)
##    kset.to_netcdf('~/scratch/python/punjab_17_conf_h_n_winter_dist_time_computed.nc')
    file.write('\n File export of Dist time comp starts......')
    df_punjab_17_conf_h_n_winter_dist_computed.to_csv('~/scratch/python/sikim_multi/df_cf_corr_punjab_17_conf_h_n_winter_dist_time_computed.csv')
    file.write('\n program finished successfully ....')
    file.close()
ntime=1
t_init = time.time()
for c_time in range(ntime):
    main_code()
t_fnl=time.time()
total_c_time=t_fnl - t_init
total_c = total_c_time/ntime
print(total_c)

