from datetime import datetime
from dateutil import parser
import math
import numpy as np
import pandas as pd
import numpy as np
from numba import cuda, jit

@cuda.jit(device=True)
def haversine_cuda(s_lat,s_lng,e_lat,e_lng):
    
    R = 6371.009

    s_lat = s_lat * math.pi / 180                     
    s_lng = s_lng * math.pi / 180 
    e_lat = e_lat * math.pi / 180                    
    e_lng = e_lng * math.pi / 180 

    d = math.sin((e_lat - s_lat)/2)**2 + math.cos(s_lat)*math.cos(e_lat) * math.sin((e_lng - s_lng)/2)**2

    return 2 * R * math.asin(math.sqrt(d))


@cuda.jit()
def compute_pairwise_distances_and_time(coord1lat,coord1lon,coord2lat,coord2lon, date1, date2, max_dist, out1, out2, out3, out4, out5, out6, out7, out8):
    
    start = cuda.grid(1)
    stride = cuda.gridsize(1)
    
    for i in range(start, coord1lat.shape[0], stride):
        _lat1 = coord1lat[i]
        _lng1 = coord1lon[i]
        _lat2 = coord2lat[i]
        _lng2 = coord2lon[i]
        _d1 = date1[i]
        _d2 = date2[i]
         
        dist = haversine_cuda(_lat1, _lng1, _lat2, _lng2)
        time_diff = math.fabs((_d1-_d2)/86400.0)
        
        if dist<max_dist:
            out1[i] = _lat1
            out2[i] = _lng1
            out3[i] = _lat2
            out4[i] = _lng2
            out5[i] = dist
            out6[i] = _d1
            out7[i] = _d2
            out8[i] = time_diff

cz = 10 ** 7
loop = 0
for chunk in (pd.read_csv('/home/cas/irdstaff/ird12843/scratch/python/sikim_multi/text_punjab_comb_dataframe.txt', chunksize=cz)):
    loop = loop+1
    #print('Chunk',chunk.head())
    chunk = chunk.astype('float32')
    #print('Chunk',chunk.info())
    #chunk.to_csv('/home/cas/irdstaff/ird12843/scratch/python/sikim_multi/Chunk_test.csv',index=False)
    index = chunk.Index.values
    threads_per_block = 512
    blocks_per_grid = 36
    com_length = len(index)

    coord1lat_gpu = cuda.to_device(chunk.id_lat.values)
    coord1lon_gpu = cuda.to_device(chunk.id_lon.values)
    coord2lat_gpu = cuda.to_device(chunk.fid_lat.values)
    coord2lon_gpu = cuda.to_device(chunk.fid_lon.values)
    acqdate1_gpu = cuda.to_device(chunk.id_acq_date.values)
    acqdate2_gpu = cuda.to_device(chunk.fid_acq_date.values)
    out_gpu_lat_1 = cuda.device_array(shape=(com_length,), dtype=np.float32)
    out_gpu_lng_1 = cuda.device_array(shape=(com_length,), dtype=np.float32)
    out_gpu_lat_2 = cuda.device_array(shape=(com_length,), dtype=np.float32)
    out_gpu_lng_2 = cuda.device_array(shape=(com_length,), dtype=np.float32)
    out_gpu_dist = cuda.device_array(shape=(com_length,), dtype=np.float32)
    out_gpu_acqdate_1 = cuda.device_array(shape=(com_length,), dtype=np.float32)
    out_gpu_acqdate_2 = cuda.device_array(shape=(com_length,), dtype=np.float32)
    out_gpu_time_diff = cuda.device_array(shape=(com_length,), dtype=np.float32)
    #print('computation Finished')

    
    compute_pairwise_distances_and_time[blocks_per_grid, threads_per_block](coord1lat_gpu,coord1lon_gpu,coord2lat_gpu,coord2lon_gpu, acqdate1_gpu, acqdate2_gpu, 1000, out_gpu_lat_1, out_gpu_lng_1, out_gpu_lat_2, out_gpu_lng_2, out_gpu_dist, out_gpu_acqdate_1, out_gpu_acqdate_2, out_gpu_time_diff)
    ##(out_gpu_dist.copy_to_host())
    ##out_gpu_time_diff.copy_to_host()
    df_trunc_computed = pd.DataFrame(
        {'Index': index,
         'latitude_id': out_gpu_lat_1.copy_to_host(),
         'longitude_id': out_gpu_lng_1.copy_to_host(),
         'latitude_fid': out_gpu_lat_2.copy_to_host(),
         'longitude_fid': out_gpu_lng_2.copy_to_host(),
         'dist': out_gpu_dist.copy_to_host(),
         'acq_date_id': out_gpu_acqdate_1.copy_to_host(),
         'acq_date_fid': out_gpu_acqdate_2.copy_to_host(),
         'time_diff': out_gpu_time_diff.copy_to_host()
        })
    #print('Shape of output',df_trunc_computed.shape)
    print(loop)
    if (loop==1):
        df_trunc_computed.to_csv('~/scratch/python/sikim_multi/df_cuda_punjab_17_conf_h_n_winter_dist_time_computed.csv',index=False)
    if(loop>1):
        df_trunc_computed.to_csv('~/scratch/python/sikim_multi/df_cuda_punjab_17_conf_h_n_winter_dist_time_computed.csv', mode='a',index=False, header=False)
        #break
