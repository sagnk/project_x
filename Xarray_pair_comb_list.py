import pandas as pd
from geopy import distance
import numpy as np
import itertools
from datetime import datetime
import xarray as xr
file = open('Stage1.txt','w') 
df_punjab_17_conf_h_n_winter = pd.read_csv('~/scratch/python/viirs_fire-master/data/processed/csv/punjab_17_conf_n_h_winter.csv').drop('Unnamed: 0',axis=1)
df_punjab_17_conf_h_n_winter['id'] = df_punjab_17_conf_h_n_winter.index
#df_punjab_17_conf_h_n_winter = df_punjab_17_conf_h_n_winter[0:20]
xr_punjab_17_conf_h_n_winter = df_punjab_17_conf_h_n_winter.to_xarray()
ll = np.arange(len(df_punjab_17_conf_h_n_winter['id']))
combs = list(itertools.combinations(ll,2))
lenght_combinations = len(combs)
print(lenght_combinations)
file.write('length of combination list:')
file.write(str(lenght_combinations))
file.close()
#df = pd.DataFrame(combs)
file = open('Stage2.txt','w')
#df.to_csv('~/scratch/python/combination_list.csv')
#file.write('Combination list csv exported successfully')
list_id = []
list_fid = []
list_dist = []
file.write('\n Loop for distance computation starts:')
l=0
for lat_long_pair in combs:
    file.write(str(l))
    file.write(',')
    if (l%10000==0):
        file.close()
        file = open('Stage2.txt','a')
    l=l+1
    temp_id_lat_long = float(xr_punjab_17_conf_h_n_winter.latitude[lat_long_pair[0]].values),float(xr_punjab_17_conf_h_n_winter.longitude[lat_long_pair[0]].values)
    temp_fid_lat_long = float(xr_punjab_17_conf_h_n_winter.latitude[lat_long_pair[1]].values),float(xr_punjab_17_conf_h_n_winter.longitude[lat_long_pair[1]].values)

    if (distance.geodesic(temp_id_lat_long,temp_fid_lat_long))<100:
        list_id.append(lat_long_pair[0])
        list_fid.append(lat_long_pair[1])
        temp_val = float(str(distance.great_circle(temp_id_lat_long,temp_fid_lat_long))[:-4])
        list_dist.append(round(temp_val,4))
file.write('\n Loop for distance computation finished')
file.close()
file = open('Stage3.txt','w')
file.write('Dataframe creation for distance computation Starts')
#df_punjab_17_conf_h_n_winter_dist_computed = pd.DataFrame(
#    {'id': list_id,
#     'fid': list_fid,
#     'dist': list_dist
#    })
#punjab_17_conf_h_n_winter_dist_computed = [list_id,list_fid,list_dist]
file.write('\n Dataframe creation for distance computation Finished')
file.write('\n File export for distance dataframe starts')
#df_punjab_17_conf_h_n_winter_dist_computed.to_csv('~/scratch/python/df_punjab_17_conf_h_n_winter_dist_computed.csv')
file.write('\n File export for distance dataframe Finished')
tm = pd.to_datetime(xr_punjab_17_conf_h_n_winter.acq_date.values)
time = [xr_punjab_17_conf_h_n_winter.index.values,tm]
time_diff_days = []
file.close()
file = open('Stage4.txt','a')
file.write('\n Time difference loop starts:')
list_len = len(list_id)
for i in range(0,list_len):
    file.write(str(i))
    file.write(',')
    if (l%10000==0):
        file.close()
        file = open('Stage4.txt','a')
    time_diff_days.append(abs((time[1][list_id[i]]-time[1][list_fid[i]])).days)
file.write('\n Time difference loop finishes ......')
file.close()
file = open('Stage5.txt','a')
kset = xr.Dataset()
index_range = np.arange(list_len)
kset['index'] = (('index'),index_range)
kset['id'] = (('index'),list_id)
kset['fid'] = (('index'),list_fid)
kset['dist'] = (('index'),list_dist)
kset['time_diff_days'] = (('index'),time_diff_days)
kset.to_netcdf('~/scratch/python/punjab_17_conf_h_n_winter_dist_time_computed.nc')
file.write('\n File export of Dist time comp starts......')
#df_punjab_17_conf_h_n_winter_dist_computed.to_csv('~/scratch/python/df_punjab_17_conf_h_n_winter_dist_time_computed.csv')
file.write('\n program finished successfully ....')
file.close()
